import React from 'react';
import { Text, View, TextInput, TouchableOpacity } from 'react-native';
import styles from './app/views/styles/allStyles';


export default class App extends React.Component {

  constructor(props){
    super(props);
    this.state={text: ''};
    this._onChangeText = this._onChangeText.bind(this);
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput  style={styles.textInput} placeholder="type something here." onChangeText={this._onChangeText} />
        <Text style={styles.text}>
         {this.state.text.split(' ').map( word => word && '1').join(' ')}
        </Text>
        <TouchableOpacity onPress={() => {console.log('share');}}>
          <View style={styles.shareImage}>
            <Text>Share Image</Text>    
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  _onChangeText(text){
    this.setState({text});
  }


}