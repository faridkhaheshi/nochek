import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'yellow',
    paddingTop: 50,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 20,
  },
  textInput:{
  	height: 40,
  },
  text:{
  	color: 'red',
  	padding: 10,
  	fontSize: 42,
  },
  shareImage:{
    width: 100,
    height: 500,
    backgroundColor: 'red',
  },
});

export default styles;